<?php

class Node
{
    /** @var int */
    private $data;

    /** @var Node */
    private $next;

    public function getNext(): ?Node
    {
        return $this->next;
    }

    public function setNext(?Node $next): void
    {
        $this->next = $next;
    }

    public function getData(): int
    {
        return $this->data;
    }

    public function setData(int $data): void
    {
        $this->data = $data;
    }
}

function createNode(int $data): Node
{
    $n = new Node();

    $n->setData($data);

    return $n;
}

function pushNode(?Node $current, int $data): Node
{
    if ($current === null) {
        return null;
    }

    $n = createNode($data);

    $current->setNext($n);

    return $n;
}

function createLinkedList($size): Node
{
    if ($size < 1) {
        return null;
    }

    $head = createNode(0);
    $current = $head;

    for ($i = 1; $i < $size; $i++) {
        $current = pushNode($current, $i);
    }

    return $head;
}

function printLinkedList(Node $head): void
{
    $curr = $head;

    while ($curr !== null) {
        echo $curr->getData();
        $curr = $curr->getNext();
    }

    echo PHP_EOL;
}

/**
 * @param Node $head
 * @return Node[]
 */
function findTail(Node $head): array
{
    $curr = $head;
    $prev = null;
    while ($curr !== null) {

        if ($curr->getNext() === null) {
            return [$prev, $curr];
        }

        $prev = $curr;
        $curr = $curr->getNext();
    }

    return [$prev, $curr];
}

function reverseList(Node $head): Node
{
    /** @var Node $newHead */
    $newHead = null;

    while ($head->getNext()) {
        $tail = findTail($head);
        echo $tail[0]->getData() . '->' . $tail[1]->getData() . PHP_EOL;

        // cut previous connection
        $tail[0]->setNext(null);

        if ($newHead === null) {
            $tail[1]->setNext(null);
            $newHead = $tail[1];
        } else {
            findTail($newHead)[1]->setNext($tail[1]);
        }
    }

    findTail($newHead)[1]->setNext($head);

    return $newHead;
}

function main(): void
{
    $head = createLinkedList(10);
    printLinkedList($head);

    $head = reverseList($head);
    printLinkedList($head);
}

main();

<?php

class Node
{
    /** @var int */
    private $data;

    /** @var Node */
    private $next;

    public function getNext()
    {
        return $this->next;
    }

    public function setNext($next)
    {
        $this->next = $next;
    }

    public function getData(): int
    {
        return $this->data;
    }

    public function setData(int $data)
    {
        $this->data = $data;
    }
}

function createNode(int $data): Node
{
    $n = new Node();

    $n->setData($data);

    return $n;
}

function pushNode($current, int $data): Node
{
    if ($current === null) {
        return null;
    }

    $n = createNode($data);

    $current->setNext($n);

    return $n;
}

function createLinkedList($size): Node
{
    if ($size < 1) {
        return null;
    }

    $head = createNode(0);
    $current = $head;

    for ($i = 1; $i < $size; $i++) {
        $current = pushNode($current, $i);
    }

    return $head;
}

function printLinkedList(Node $head)
{
    $curr = $head;

    while ($curr !== null) {
        echo $curr->getData();
        $curr = $curr->getNext();
    }

    echo PHP_EOL;
}

/**
 * @param Node $head
 * @return Node[]
 */
function findTail(Node $head): array
{
    $curr = $head;
    $prev = null;
    while ($curr !== null) {

        if ($curr->getNext() === null) {
            return [$prev, $curr];
        }

        $prev = $curr;
        $curr = $curr->getNext();
    }

    return [$prev, $curr];
}

function reverseList(Node &$head)
{
    $prev = null;
    $curr = $head;

    while ($curr !== null) {
        $next = $curr->getNext();
        $curr->setNext($prev);
        $prev = $curr;
        $curr = $next;
    }

    $head = $prev;
}

function main()
{
    $head = createLinkedList(10);
    printLinkedList($head);

    reverseList($head);
    printLinkedList($head);
}

main();
